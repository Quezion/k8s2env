(ns k8s2env.core
  (:gen-class)
  (:require [yaml.core :as yaml]
            ;;[slingshot.slingshot :refer [try+ throw+]]
            ))

(defn kv->envlist-line
  [k v]
  (as-> (clojure.string/escape v {\" "\\\""}) <>
    (str k "=" "\"" <> "\"")))

(defn m-name-value->envlist-line
  "Given input map with keys :name :value, returns formatted string in env.list format"
  [{:keys [name value] :as envmap}]
  (as-> (clojure.string/escape value {\" "\\\""}) <>
    (str name "=" "\"" <> "\"")))

(defn deployment-tree->m-name-values
  "Given hashmap representing YAML deployment, returns a map of env"
  [m]
  (assert (= (:apiVersion m) "apps/v1beta1") "only known to work with apiVersion apps/v1beta")
  (let [containers (get-in m [:spec :template :spec :containers])
        container (first containers)]
    (assert (not= 0 (count containers)) "At least one container must be defined on deployment.yaml")
    (assert (< (count containers) 2) "Only one container must be defined on deployment.yaml")
    (:env container)))

(defn deployment->envlist-lines
  [deployment]
  (->> (deployment-tree->m-name-values deployment)
       (map m-name-value->envlist-line)))

(defn configmap->envlist-lines
  [{:keys [kind apiVersion data] :as m}]
  (assert (and (= kind "ConfigMap")
               (= apiVersion "v1")) "only known to work with apiVersion v1")
  (->> (map #(vector (name (first %)) (second %)) data)
       (map #(apply kv->envlist-line %))))

(defn convert
  [template-filepath output-filepath]
  (let [aggregated-yaml (slurp template-filepath)
        split-yaml (->> (clojure.string/split aggregated-yaml #"---")
                        (remove empty?))
        k8s-resources (map yaml/parse-string split-yaml)
        deployments (filter #(-> % :kind (= "Deployment")) k8s-resources)
        _ (assert (= 1 (count deployments)) "2+ deployment resources found, not supported & quitting")
        deployment (first deployments)
        deployment-envlist-lines (deployment->envlist-lines deployment)
        configmaps (filter #(-> % :kind (= "ConfigMap")) k8s-resources)
        configmap (first configmaps)
        configmap-envlist-lines (configmap->envlist-lines configmap)]
    (->> (reduce into [deployment-envlist-lines configmap-envlist-lines])
         (interpose "\n")
         (apply str)
         (spit output-filepath))
    (println "Wrote envlist to" output-filepath)))

(def help-opts
  #{"-h"
    "--h"
    "-help"
    "--help"})

(defn -main
  [& args]
  (if (contains? help-opts (first args))
    (println "1st argument is string filepath to input templated.yaml\n"
             "-- it must contain one deployment & may contain one configmap\n"
             "2nd argument is string filepath for output env.list\n"
             "You can probably obtain your output templated.yaml with command like:\n"
             "helm template service -f service/values-rqa3.yaml > templated.yaml")
    (if (and (= 2 (count args))
             (and (string? (first args)) (string? (second args))))
      (convert (first args) (second args))
      (println "Invalid number/type of arguments. Expected 2 string filepaths but got " (count args) " arguments"))))
