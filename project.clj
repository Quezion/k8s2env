(defproject k8s2env "0.1.0"
  :description "Given a k8 template, scrapes all pod env & outputs to env.list"
  :url "http://concur.github.com"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 
                 ;; yaml parsing
                 [io.forward/yaml "1.0.9"]
                 
                 ;; Enhanced try/catch for Clojure
                 ;;[slingshot "0.12.2"]
                 ]
  :main ^:skip-aot k8s2env.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
